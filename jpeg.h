#include <stdint.h>

#ifndef JPEG_H
#define JPEG_H

#define COMPONENT_Y 			1
#define COMPONENT_CB			2
#define COMPONENT_CR			3

typedef struct HTree* HTreePtr_t;

#define GET_LOW_NIBBLE(b)  ((b) & 0xF)
#define GET_HIGH_NIBBLE(b) (((b)>>4) & 0xF)

#pragma pack(push, 1)

struct JPGHeader_JFIF
    {
    char identifier[5]; /*Should read JFIF\0 */
    uint16_t version;
    uint8_t units;
    /*As quoted from the web site about the units variable:
    *0 => no units, X and Y specify pixel aspect ratio
    *1 => X and Y are dots per inch
    *2 => X and Y are dots per cm  */
    uint16_t Xdensity;
    uint16_t Ydensity;
    uint8_t Xthumbnail;
    uint8_t Ythumbnail;
    };

struct QTable
    {
    uint8_t pindex; /*Precision higher four bits, index lower four bits*/
    /*8-bit for precision 0, 16 bit for precision 1*/
    uint8_t qValues[64]; /*Quantization table, stored in zigzag format*/
    };

// Start of frame header
struct JPGHeader_SOF
    {
    uint8_t P; /*Sample precision in bits*/
    uint16_t height;
    uint16_t width;
    uint8_t components;
    /*The following variables contain information about
    *the different components of the image.  There is 1 component
    *for a gray-scale image and 3 components for a color image.
    *ID variable: The ID of the component
    *HVSample: Horizontal sampling rate (high nibble) and vertical
    *sampling rate (low nibble)
    *QTable: Quantization table number*/
    /*NOTE: YCbCr color used*/
    uint8_t c1ID;
    uint8_t c1HVSample;
    uint8_t c1QTable;
    uint8_t c2ID;
    uint8_t c2HVSample;
    uint8_t c2QTable;
    uint8_t c3ID;
    uint8_t c3HVSample;
    uint8_t c3QTable;
    };

struct JPGHeader_SOS
    {
    uint8_t components;
    uint8_t c1ID;
    /*DC (high nibble) and AC (low nibble) table numbers*/
    uint8_t c1HTable;
    uint8_t c2ID;
    uint8_t c2HTable;
    uint8_t c3ID;
    uint8_t c3HTable;
    uint8_t Ss;
    uint8_t Se;
    uint8_t AhAl;
    };

#pragma pack(pop)

/*NOTE: A DHT segment can contain multiple headers, each with
*their own index, bits, and Huffman values, or they can all be
*defined separately*/
struct HuffmanTable
    {
    // index: high nibble (0) DC table (1) AC Table, low nibble: ID
    uint8_t index;
    // bits: How many different codewords of each length there are,
    // starting at length 1
    uint8_t* pBits;
    // The Huffman values associated with each codeword.  The
    // number of bytes is the sum of all of the values in bits
    // (we need a byte to represent the value of every codeword
    // that has a mapping in this table)
    uint8_t bindings;
    uint8_t* pHValues;
    HTreePtr_t hTree;
    };

typedef struct JPGObj* JPGObj_T;

typedef struct COEFF
    {
    uint16_t values[64];
    } coefficients_t;

/*This structure will bring together a lot of the information from
*the header and organize it better*/
struct blockObject
    {
    uint8_t hSample;
    uint8_t vSample;
    HTreePtr_t dcTree;
    HTreePtr_t acTree;
    struct QTable* qTable;
    };

uint8_t Jpeg_GetByteF();
uint8_t Jpeg_EOF();


#endif // JPEG_H

