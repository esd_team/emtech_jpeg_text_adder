// JpegLuminance.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cinttypes>
#include "format.h"
#include "jpeg.h"
#include <iomanip>

using namespace std;

enum MarkerType_t
    {
    SOI = 0xD8,
    SOF0 = 0xC0,
    SOF2 = 0xC2,
    DHT = 0xC4,
    DQT = 0xDB,
    DRI = 0xDD,
    SOS = 0xDA,
    RST0 = 0xD0,
    RST1 = 0xD1,
    RST2 = 0xD2,
    RST3 = 0xD3,
    RST4 = 0xD4,
    RST5 = 0xD5,
    RST6 = 0xD6,
    RST7 = 0xD7,
    APP0 = 0xE0,
    APP1 = 0xE1,
    APP2 = 0xE2,
    APP3 = 0xE3,
    APP4 = 0xE4,
    APP5 = 0xE5,
    APP6 = 0xE6,
    APP7 = 0xE7,
    APP8 = 0xE8,
    APP9 = 0xE9,
    APPA = 0xEA,
    APPB = 0xEB,
    APPC = 0xEC,
    APPD = 0xED,
    APPE = 0xEE,
    APPF = 0xEF,
    COM = 0xFE,
    EOI = 0xD9,
    PAD = 0xFF
    };

string MarkerTypeToString(MarkerType_t mt);
bool MarkerTypeHasPayload(MarkerType_t mt);
void ReadMarker(MarkerType_t& mt, int& length);
void ProcessMarker(MarkerType_t mt, int length);
void DisplayDiagData();
void AddLinesToBottomOfImage(uint8_t* pFileData, int fileLength);
void HexDump(uint8_t* buffer, int length, int addressStart = 0, bool printAscii = false);

static void readJFIF(int length);
static void readDQT(int length);
static void readDHT(int length);
static void readSOF(int length);
static void readSOS(int length);

uint8_t* vlFileDataPtr;
int vlFileLength;
int vlFilePos;
int vlGreatestHSample;
int vlGreatestVSample;
int vlMcuX;
int vlMcuY;
// Sized for 2592x1944 max
coefficients_t vlCoefficients[3][78732];

struct JPGHeader_JFIF* vlJpegHeaderPtr;

#define MAX_QTABLES     4
int vlQTableCount;
struct QTable* vlQTablesPtr[MAX_QTABLES];

#define MAX_HTABLES     4
int vlHTableCount;
struct HuffmanTable vlHTables[MAX_HTABLES];

struct JPGHeader_SOF* vlSofPtr;
struct JPGHeader_SOS* vlSosPtr;

struct blockObject vlComponents[3];


int main(int argc, char* argv[])
    {
    if ((argc < 2) || (argc > 3))
        {
        cout << "Usage:" << endl;
        cout << "   JpegTextAdder <filename>" << endl;
        return(0);
        }
    // Try to open the file
    ifstream theFile;
    theFile.open(argv[1], ios::binary | ios::in);
    if (!theFile.good())
        {
        theFile.close();
        cout << "ERROR: input file could not be opened." << endl;
        return(1);
        }

    // Read entire file into memory
    try
        {
        // Get length of file:
        theFile.seekg(0, theFile.end);
        vlFileLength = (int)theFile.tellg();
        theFile.seekg(0, theFile.beg);
        // allocate memory:
        vlFileDataPtr = new uint8_t[vlFileLength];
        // read data as a block:
        theFile.read(reinterpret_cast<char*>(vlFileDataPtr), vlFileLength);
        theFile.close();
        }
    catch (exception& e)
        {
        cout << e.what() << endl;
        theFile.close();
        return(2);
        }

    // Process the data
    try
        {
        MarkerType_t mt;
        int length;
        vlFilePos = 0;
        vlHTableCount = 0;
        vlQTableCount = 0;
        // First marker must be SOI (start of image)
        int startOfMarker = vlFilePos;
        ReadMarker(mt, length);
        fmt::print("{0:4s} at {1:#06X} length {2}\n", MarkerTypeToString(mt), startOfMarker, length);
        HexDump(&vlFileDataPtr[startOfMarker], (vlFilePos - startOfMarker), startOfMarker, true);
        if (mt != SOI)
            {
            throw runtime_error(fmt::format("Expected SOI marker at {:#06X}", startOfMarker));
            }
        ProcessMarker(mt, length);
        // Process the remaining markers until we see SOS
        while ((vlFilePos < vlFileLength) && (mt != SOS))
            {
            startOfMarker = vlFilePos;
            ReadMarker(mt, length);
            fmt::print("{0:4s} at {1:#06X} length {2}\n", MarkerTypeToString(mt), startOfMarker, length);
            HexDump(&vlFileDataPtr[startOfMarker], (vlFilePos - startOfMarker), startOfMarker, true);
            ProcessMarker(mt, length);
            }
        // Dump some diagnostics
        DisplayDiagData();

        // Add 16 lines to the bottom of the image
        AddLinesToBottomOfImage(vlFileDataPtr, vlFileLength);
        }
    catch (exception& e)
        {
        cout << e.what() << endl;
        }

    // Release the data
    delete[] vlFileDataPtr;
    return 0;
    }

/**
 * Add 16 lines of image data to the bottom of the JPEG file in memory.
 * The lines should be black/white data only (luminance).
 * For now the 16 lines can be any interesting pattern. Eventually it will be text data.
 * 
 * General idea is to scan backwards in the file to the EOI marker and save the data from the
 * EOI marker to the end of the file.
 * Starting at the old EOI marker, overwrite the existing JPEG data with:
 *   Add a RST marker to restart the block-to-block predictor values.
 *   For each MCU in each line:
 *      Add luminance MCU
 *      Add empty chrominance MCUs
 *
 * pFileData    Points to the beginning of the JPEG image data buffer.
 * fileLength   Total length of the image data in the image buffer.
 * sosOffset    Gives position relative to start of image buffer for the SOS structure.
 **/
void AddLinesToBottomOfImage(uint8_t* pFileData, int fileLength)
    {
    // The following may be useful:
    //
    // vlSofPtr         points to start of frame structure
    // vlSosPtr         points to Start Of Scan structure
    // vlQTableCount    Number of QTables
    // vlHTableCount    Number of HTables
    // vlQTablesPtr     Array of pointers to QTables
    // vlHTables        Array of HTables
    //
    }


void ProcessMarker(MarkerType_t mt, int length)
    {
    HexDump(&vlFileDataPtr[vlFilePos], length, vlFilePos, true);
    switch (mt)
        {
        case SOI:
            // Nothing to do.
            break;
        case APP0:
            readJFIF(length);
            break;
        case APP1:
        case DRI:
            vlFilePos += length;
            break;
        case DQT:
            readDQT(length);
            break;
        case SOF0:
            readSOF(length);
            break;
        case DHT:
            readDHT(length);
            break;
        case SOS:
            readSOS(length);
            break;
        default:
            // Unsupported marker type
            throw runtime_error(fmt::format("Unsupported marker type '{0}' at {1:#06X}",
                                            MarkerTypeToString(mt), vlFilePos));
        }
    }

void DisplayDiagData()
    {
    for (int i = 0; i < vlQTableCount; i++)
        {
        printf("QTable %x\n", vlQTablesPtr[i]->pindex);
        for (int j = 0; j < 64; j++)
            {
            printf("%i ", vlQTablesPtr[i]->qValues[j]);
            }
        printf("\n\n");
        }
    printf("\n\n");
    for (int i = 0; i < 4; i++)
        {
        int sum = 0;
        printf("Huffman Table %x:\n", vlHTables[i].index);
        printf("Bits: {");
        for (int j = 0; j < 16; j++)
            {
            printf("%i,", vlHTables[i].pBits[j]);
            sum += vlHTables[i].pBits[j];
            }
        printf("}\nHuffman Byte Values: {");
        for (int j = 0; j < sum; j++)
            {
            printf("%x,", vlHTables[i].pHValues[j]);
            }
        printf("}\n\n");
        }
    printf("width = %i\n", vlSofPtr->width);
    printf("height = %i\n", vlSofPtr->height);

    printf("\n(Component ID, HorizontalSample/VerticalSample, Quantization table number, DC Table/AC Table)\n");
    printf("(%i, %x, %i, %x)\n", vlSofPtr->c1ID, vlSofPtr->c1HVSample, vlSofPtr->c1QTable, vlSosPtr->c1HTable);
    printf("(%i, %x, %i, %x)\n", vlSofPtr->c2ID, vlSofPtr->c2HVSample, vlSofPtr->c2QTable, vlSosPtr->c2HTable);
    printf("(%i, %x, %i, %x)\n", vlSofPtr->c3ID, vlSofPtr->c3HVSample, vlSofPtr->c3QTable, vlSosPtr->c3HTable);
    }


/**
* If the byte is 0xFF, check to make sure the next byte is 0x00
* before returning 0xFF.  Otherwise, totally skip the two 0xFFxx
* (probably for alignment purposes, and to be ignored)
**/
uint8_t Jpeg_GetByteF()
    {
    uint8_t b = vlFileDataPtr[vlFilePos++];
    if (b == 0xFF)
        {
        b = vlFileDataPtr[vlFilePos++];
        if (b != 0x00)
            {
            return vlFileDataPtr[vlFilePos++];
            }
        return 0xFF;
        }
    return b;
    }

uint8_t Jpeg_EOF()
    {
    return(vlFilePos >= vlFileLength);
    }

string MarkerTypeToString(MarkerType_t mt)
    {
    switch (mt)
        {
        case SOI: return("SOI");
        case SOF0: return("SOF0");
        case SOF2: return("SOF2");
        case DHT: return("DHT");
        case DQT: return("DQT");
        case DRI: return("DRI");
        case SOS: return("SOS");
        case RST0: return("RST0");
        case RST1: return("RST1");
        case RST2: return("RST2");
        case RST3: return("RST3");
        case RST4: return("RST4");
        case RST5: return("RST5");
        case RST6: return("RST6");
        case RST7: return("RST7");
        case APP0: return("APP0");
        case APP1: return("APP1");
        case APP2: return("APP2");
        case APP3: return("APP3");
        case APP4: return("APP4");
        case APP5: return("APP5");
        case APP6: return("APP6");
        case APP7: return("APP7");
        case APP8: return("APP8");
        case APP9: return("APP9");
        case APPA: return("APPA");
        case APPB: return("APPB");
        case APPC: return("APPC");
        case APPD: return("APPD");
        case APPE: return("APPE");
        case APPF: return("APPF");
        case COM: return("COM");
        case EOI: return("EOI");
        case PAD: return("PAD");
        }
    return("unknown");
    }

bool MarkerTypeHasPayload(MarkerType_t mt)
    {
    switch (mt)
        {
        case SOI: return(false);
        case SOF0: return(true);
        case SOF2: return(true);
        case DHT: return(true);
        case DQT: return(true);
        case DRI: return(true);
        case SOS: return(true);
        case RST0: return(false);
        case RST1: return(false);
        case RST2: return(false);
        case RST3: return(false);
        case RST4: return(false);
        case RST5: return(false);
        case RST6: return(false);
        case RST7: return(false);
        case APP0: return(true);
        case APP1: return(true);
        case APP2: return(true);
        case APP3: return(true);
        case APP4: return(true);
        case APP5: return(true);
        case APP6: return(true);
        case APP7: return(true);
        case APP8: return(true);
        case APP9: return(true);
        case APPA: return(true);
        case APPB: return(true);
        case APPC: return(true);
        case APPD: return(true);
        case APPE: return(true);
        case APPF: return(true);
        case COM: return(true);
        case EOI: return(false);
        case PAD: return(false);
        }
    return(false);
    }

void ReadMarker(MarkerType_t& mt, int& length)
    {
    if (vlFileDataPtr[vlFilePos++] != 0xFF)
        {
        throw runtime_error(fmt::format("Expected start of marker at {:#06X}", vlFilePos - 1));
        }
    mt = static_cast<MarkerType_t>(vlFileDataPtr[vlFilePos++]);
    length = 0;
    if (MarkerTypeHasPayload(mt))
        {
        length = ((vlFileDataPtr[vlFilePos] << 8) + vlFileDataPtr[vlFilePos + 1]) - 2;
        if ((length < 0) || (length > (vlFileLength - vlFilePos)))
            {
            throw runtime_error(fmt::format("Invalid marker payload length at {:#06X}", vlFilePos));
            }
        vlFilePos += 2;
        }
    }

// Return a uint16_t equivalent to value with its endianness swapped
uint16_t endianSwapW(uint16_t value)
    {
    return (((value & 0xFF00) >> 8) | ((value & 0x00FF) << 8));
    }

static void readJFIF(int length)
    {
    vlJpegHeaderPtr = (JPGHeader_JFIF*)&vlFileDataPtr[vlFilePos];
    // To prevent buffer overrun attacks
    if (length > sizeof(struct JPGHeader_JFIF))
        {
        printf("ERROR: JFIF Header is too big\n");
        vlJpegHeaderPtr = NULL;
        return;
        }
    vlFilePos += length;
    // Words version, XDensity, YDensity have endian issues.
    vlJpegHeaderPtr->version = endianSwapW(vlJpegHeaderPtr->version);
    vlJpegHeaderPtr->Xdensity = endianSwapW(vlJpegHeaderPtr->Xdensity);
    vlJpegHeaderPtr->Ydensity = endianSwapW(vlJpegHeaderPtr->Ydensity);
    }

static void readDQT(int length)
    {
    uint16_t size = sizeof(struct QTable);
    uint16_t toAdd = length / size;
    if ((toAdd * size) != length)
        {
        printf("ERROR: DQT table size not a multiple of the table structure size.\n");
        return;
        }
    while (toAdd)
        {
        vlQTablesPtr[vlQTableCount++] = (QTable*)&vlFileDataPtr[vlFilePos];
        vlFilePos += size;
        toAdd--;
        }
    }

static void readDHT(int length)
    {
    uint16_t bytesRead = 0;
    int i;
    struct HuffmanTable* pCurrentTable;

    while (bytesRead < length)
        {
        if (vlHTableCount >= MAX_HTABLES)
            {
            printf("ERROR: Greater than four Huffman tables present.\n");
            return;
            }
        pCurrentTable = &vlHTables[vlHTableCount];
        // Read in the index and bits[16] fields
        pCurrentTable->index = vlFileDataPtr[vlFilePos++];
        pCurrentTable->pBits = &vlFileDataPtr[vlFilePos];
        vlFilePos += 16;
        // Add up all of the values in bits to determine how many bindings
        // there are in this Huffman table.
        pCurrentTable->bindings = 0;
        for (i = 0; i < 16; i++)
            {
            pCurrentTable->bindings += pCurrentTable->pBits[i];
            }
        pCurrentTable->pHValues = &vlFileDataPtr[vlFilePos];
        vlFilePos += pCurrentTable->bindings;
        bytesRead += (17 + pCurrentTable->bindings);
        vlHTableCount++;
        }
    }

static void readSOF(int length)
    {
    // To prevent buffer overrun attacks
    if (length > sizeof(struct JPGHeader_SOF))
        {
        printf("ERROR: Start of Frame Header is too big.\n");
        vlSofPtr = NULL;
        return;
        }
    vlSofPtr = (JPGHeader_SOF*)&vlFileDataPtr[vlFilePos];
    vlFilePos += length;
    // Words height, width have endian issues
    vlSofPtr->height = endianSwapW(vlSofPtr->height);
    vlSofPtr->width = endianSwapW(vlSofPtr->width);
    }

static void readSOS(int length)
    {
    // To prevent buffer overrun attacks
    if (length > sizeof(struct JPGHeader_SOS))
        {
        printf("ERROR: Start of Scan Header is too big: %i, %i\n", length, sizeof(struct JPGHeader_SOS));
        vlSosPtr = NULL;
        return;
        }
    vlSosPtr = (JPGHeader_SOS*)&vlFileDataPtr[vlFilePos];
    vlFilePos += length;
    }

void HexDump(uint8_t* buffer, int length, int addressStart, bool printAscii)
    {
    int position = 0;
    long int addr;
    long int cnt;
    long int cnt2;
    long n;

    addr = addressStart;
    while (1)
        {
        // Grab 16 bytes at a time.
        cnt = min(16, (length - position));
        if (cnt <= 0)
            {
            break;
            }
        // Print the address.
        cout << setfill('0') << hex << setw(7) << (int)addr << "  ";
        addr = addr + 16;
        // Print 16 data items, in pairs, in hexadecimal.
        cnt2 = 0;
        for (n = 0; n < 16; n++)
            {
            cnt2 = cnt2 + 1;
            if (cnt2 <= cnt)
                {
                cout << hex << setw(2) << setfill('0') << (int)buffer[n + position];
                }
            else
                {
                cout << "  ";
                }
            cout << " ";
            }
        cout << setfill(' ');
        // Print the printable characters, or a period if unprintable.
        if (printAscii)
            {
            cout << " ";
            cnt2 = 0;
            for (n = 0; n < 16; n++)
                {
                cnt2 = cnt2 + 1;
                if (cnt2 <= cnt)
                    {
                    if (buffer[n+position] < 32 || 126 < buffer[n+position])
                        {
                        cout << '.';
                        }
                    else
                        {
                        cout << buffer[n+position];
                        }
                    }
                }
            }
            cout << "\n";
            cout << dec;

            position += cnt;
            if (position >= length)
            {
            break;
            }
        }
    }
